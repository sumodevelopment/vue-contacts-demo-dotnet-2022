import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import { Contact } from "./api/contacts";

export interface State {
	contacts: Contact[];
}

export const key: InjectionKey<Store<State>> = Symbol();

export default createStore<State>({
	state: {
		contacts: [],
	},
	mutations: {
		setContacts: (state: State, payload: Contact[]) => {
			state.contacts = [...payload];
		},
		setAsFavourite: (state: State, contactId: string) => {
			for (const contact of state.contacts) {
				if (contact.login.uuid === contactId) {
					contact.isFavourite = true;
					break;
				}
			}
		},
	},
	getters: {
		contactsCount: (state: State): number => {
			return state.contacts.length;
		},
		favouritesCount: (state: State): number => {
			return state.contacts
				.filter((contact: Contact) => contact.isFavourite)
				.length;
		},
	},
});

export function useStore() {
	return baseUseStore(key);
}
