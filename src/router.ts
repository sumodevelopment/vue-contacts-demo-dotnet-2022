import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Contacts from "./pages/Contacts.vue";

const routes: RouteRecordRaw[] = [
	{
		path: "/contacts",
		component: Contacts,
	},
];

export default createRouter({
	history: createWebHistory(),
	routes,
});
