import axios from "axios";
import { MOCK_CONTACTS } from "../mocks/users.mock";

const CONTACTS_URL =
	"https://randomuser.me/api?results=20&seed=6c07b79b9a2ff1b5";

export interface ContactsResponse {
	results: Contact[];
	info: ContactsResponseInfo;
}

export interface ContactsResponseInfo {
	seed: string;
	results: number;
	page: number;
	version: string;
}

export interface Contact {
	name: ContactName;
	login: ContactLogin;
	picture: ContactPicture;
	isFavourite: boolean;
}

export interface ContactLogin {
	uuid: string;
}

export interface ContactPicture {
	medium: string;
}

export interface ContactName {
	title: string;
	first: string;
	last: string;
}

export async function findAllContacts(): Promise<[string | null, Contact[]]> {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve([null, MOCK_CONTACTS.results]);
		}, 2000);
	});

	// The actual HTTP request.
	try {
		const { data } = await axios.get<ContactsResponse>(CONTACTS_URL);
		return [null, data.results];
	} catch (error: any) {
		return [error.message, []];
	}
}
